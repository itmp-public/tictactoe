# TicTacToe - Amőba játék

A példa kód bemutatja, egy interaktív játék implemetálását JavaScript segítségével. Elöször az alkalmazás logikáját, később az interaktivitást fogjuk megvalósítani.

## A kód felépítése
* A ```tictactoe.js``` a játék logikájának az implementációját tartalmazza.

## A felhasznált beépített függvények referenciái:

### Tömb függvények
* [Array.prototype.forEach()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach) 
* [Array.prototype.filter()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) 
* [Array.prototype.reduce()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce) 
* [Array.prototype.map()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) 
* [Array.prototype.concat()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat) 