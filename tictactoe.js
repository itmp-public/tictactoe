function checkBounds(num, min, max, name) {
    if (num < min || num > max) {
        throw new Error(`${name} ${min} és ${max} között kell, hogy legyen!`);
    }
}

function filterArray(arr, testFunction) {
    let filteredArray = [];
    for (let i = 0; i < arr.length; i++) {
        let currentElement = arr[i];
        let shouldContainElement = testFunction(currentElement, i, arr);
        if (shouldContainElement) {
            filteredArray.push(currentElement);
        }
    }
    return filteredArray;
}

function reduceArray(arr, reducerFunction, initialAccumulator) {
    let accumulator = initialAccumulator;
    for (let i = 0; i < arr.length; i++) {
        let currentElement = arr[i];
        accumulator = reducerFunction(accumulator, currentElement, i, arr);
    }
    return accumulator;
}

function mapArray(arr, mappingFunction) {
    let mappedArray = [];
    for (let i = 0; i < arr.length; i++) {
        let currentElement = arr[i];
        let mappedElement = mappingFunction(currentElement, i, arr);
        mappedArray.push(mappedElement);
    }
    return mappedArray;
}

let game = {
    players: [
        { sign: "X" }, //players[0]
        { sign: "O" }],//players[1]
    board: null,
    started: false,
    ended: false,
    currentPlayer: null,
    numberOfPlacedSigns: 0,
    onPlaceSignListeners: [],
    start(player1Name, player2Name) {
        this.started = true;
        this.ended = false;
        this.numberOfPlacedSigns = 0;
        this.players[0].name = player1Name;
        this.players[1].name = player2Name;
        this.board = [[null, null, null],
        [null, null, null],
        [null, null, null]];
        this.currentPlayer = this.players[0];
        return {
            message: `${this.currentPlayer.name}, te jössz!`,
            gameEnded: false
        };
    },
    addOnPlaceSignListener(listener) {
        this.onPlaceSignListeners.push(listener);
    },
    _fireOnPlaceSignEvent(event) {
        setTimeout(() => {
            this.onPlaceSignListeners.forEach(
                listener => listener(event)
            );    
        })
    },
    placeSign(y, x) {
        console.log(`placeSign(${y}, ${x})`);
        //ellenőrzés
        if (!this.started) {
            throw new Error("A játék még el sem kezdődött, a New Game gombbal viszont megteheted!");
        } else if (this.ended) {
            return {
                message: "A játéknak már vége van, kezdj újat!",
                gameEnded: true
            };
        }
        checkBounds(y, 0, 2, 'Az y koordinata');
        checkBounds(x, 0, 2, 'Az x koordinata');
        //board update
        if (this.board[y][x]) {
            throw new Error(`Foglalt mező, ${this.currentPlayer.name}, még mindig te jössz!`);
        }
        this.board[y][x] = this.currentPlayer.sign;
        this.numberOfPlacedSigns++;

        this._fireOnPlaceSignEvent({
            sign: this.currentPlayer.sign,
            player: this.currentPlayer,
            x, y
        });

        console.log("after _fireOnPlaceSignEvent...");

        //nyert e valaki?
        let winner = this._whoWon();
        if (winner) {
            this.ended = true;
            return {
                message: `${winner.name} nyert!`,
                gameEnded: true
            };
        }
        
        if (this._isBoardFull()) {
            this.ended = true;
            return {
                message: `Döntetlen!`,
                gameEnded: true
            }
        }

        //új játékos kiválasztása

        this._selectNextPlayer();

        return {
            message: `${this.currentPlayer.name}, te jössz!`,
            gameEnded: false
        };
    },
    _selectNextPlayer() {
        this.currentPlayer =
            this.currentPlayer === this.players[0] ?
                this.players[1] : this.players[0];
    },
    _getRows() {
        return this.board;
    },
    _getColumns() {
        let cols = [];
        for (let x = 0; x < 3; x++) {
            cols.push([
                this.board[0][x],
                this.board[1][x],
                this.board[2][x]
            ]);
        }
        return cols;
    },
    _getDiagonals() {
        return [
            [this.board[0][0], this.board[1][1], this.board[2][2]],
            [this.board[2][0], this.board[1][1], this.board[0][2]],
        ];
    },
    _collectTriplets() {
        let rows = this._getRows();
        let columns = this._getColumns();
        let diagonals = this._getDiagonals();

        let triplets = [...rows, ...columns, ...diagonals];

        // for(let triplet of rows) {
        //     triplets.push(triplet);
        // }

        // for(let triplet of columns) {
        //     triplets.push(triplet);
        // }

        // for(let triplet of diagonals) {
        //     triplets.push(triplet);
        // }

        // triplets = rows.concat(columns, diagonals);

        return triplets;
    },
    _whoWon() {

        let triplets = this._collectTriplets();

        let sums = [];

        let filteredTriplets = [];

        for (let triplet of triplets) {

            let filteredTriplet = filterArray(triplet, (sign) => !!sign);
            filteredTriplets.push(filteredTriplet);

            // let sum = {};
            // for(let sign of filteredTriplet) {
            //     let currentCount = sum[sign];
            //     // sum[sign] = currentCount ? currentCount + 1 : 1;
            //     sum[sign] = (currentCount || 0) + 1;
            // }

            // let sum = reduceArray(filteredTriplet,
            //     (acc, sign) => {
            //         acc[sign]++;
            //         return acc;
            //     }, { X: 0, O: 0 });
            // sums.push(sum);
        }

        sums = filteredTriplets.map((triplet) => triplet.reduce(
            (acc, sign) => {
                acc[sign]++;
                return acc;
            }, { X: 0, O: 0 }));

        let winnerSums = sums.filter(count => count.X === 3 || count.O === 3);

        // sums = mapArray(filteredTriplets, (triplet) => reduceArray(triplet,
        //     (acc, sign) => {
        //         acc[sign]++;
        //         return acc;
        //     }, { X: 0, O: 0 }));

        // console.log("triplets: ", triplets);
        // console.log("filteredTriplets: ", filteredTriplets);
        // console.log("sums: ", sums);
        // console.log("winnerSums: ", winnerSums);

        let winners = winnerSums.map(count => count.X ? this.players[0] : this.players[1]);
        // console.log("winners: ", winners);

        let winner = winners.reduce((winner, player) => winner || player, null);
        // console.log("winner: ", winner);

        return triplets.map(triplet => triplet
            .filter(sign => !!sign) // eldobjuk az üres négyzeteket, így tehát az eredmény egy 3, 2, 1, vagy akár 0 elemű tömb attól függően, hogy hány nem null értékű mező van az eredeti tripletben.
            .reduce((count, sign) => { // megszámoljuk hány X és hány O van a hármasban (itt már nincsenek null értékű mezők, tehát lehet, hogy nem három eleme van a kérdéses "hármasnak", hanem kevesebb), pl. ['X','O','X'] -ből egy ilyen objektumot készítünk: {X:2, O:1}
                count[sign]++;
                return count;
            }, {X:0,O:0}))
            .filter(count => count.X === 3 || count.O === 3) // csak azt tartjuk meg, ahol 3 kör, vagy 3 X van
            .map(count => count.X ? this.players[0] : this.players[1]) // a nyerő játékosra mappeljük a count ojbektumot, tehát {X:3,O:0} => players[0] és {X:0,O:3} => players[1]
            .reduce((winner, player) => winner || player, null); // az első playerrel, vagy null értékkel térünk vissza, ha nincs egy nyerő játékos sem

    },
    _isBoardFull() {
        return this.numberOfPlacedSigns === 9;
    },
    toString() {
        let value = "+-------+";
        for (let y = 0; y < this.board.length; y++) {
            let row = this.board[y];
            value += "\n| ";
            for (let x = 0; x < row.length; x++) {
                let sign = row[x];
                value += sign || "."
                value += " ";
                if (x === row.length - 1) {
                    value += "|";
                }
            }
        }
        value += "\n+-------+";
        return value;
    }

};

