function onTimeoutHello() {
    console.log(`Hello, lejart az idozito...`);
}
function onTimeoutHi() {
    console.log(`Hi, lejart az idozito...`);
}

function waitSynchronously(delayInMillis) {
    console.log(`Most akkor várunk ${delayInMillis}ms-ot.`);
    const start = new Date().getTime();

    while (true) {
        const current = new Date().getTime();
        if (current - start >= delayInMillis) {
            break;
        }
    }
    console.log(`Letelt a ${delayInMillis}ms.`);
}

function onHello() {
    console.log('hello start');
    setTimeout(onTimeoutHello, 2000);
    waitSynchronously(5000);
    console.log('hello end');
}

function onHi() {
    console.log('hi start');
    setTimeout(onTimeoutHi, 2000);
    console.log('hi end');
}

document.getElementById('hello').onclick = onHello;
document.getElementById('hi').onclick = onHi;

