function mark(element) {
    element.style.border = "10px dotted red";
}

function unmark(element) {
    element.style.border = "";
}

function findCell(y, x) {
    return document.getElementById(`cell_${y}_${x}`);
}

function findAllCells() {
    return document.querySelectorAll(".cells > div.cell");
}

function clearAllCells() {
    for(let cellElement of findAllCells()) {
        setSign(cellElement, null);
    }
}

function setSign(cellElement, sign) {
    cellElement.textContent = sign;
    cellElement.classList.remove('x');
    cellElement.classList.remove('o');

    if (sign) {
        cellElement.classList.add(sign.toLowerCase());
    }
}

function setSignAt(y, x, sign) {
    let cellElement = findCell(y, x);
    setSign(cellElement, sign);
}

function setMessage(message) {
    document.querySelector(".message").textContent = message;
}

// Highlighting current player

function clearCurrentPlayer() {
    document.querySelectorAll(".player.highlight").forEach(
        elem => elem.classList.remove("highlight")
    );
}

function highlightPlayer(player) {
    let sign = player.sign;
    let signCSSClass = sign.toLowerCase();
    clearCurrentPlayer();
    document.querySelector(".player." + signCSSClass)
            .classList.add("highlight");
}

// Registering, deregistering listeners:

function registerNewGameListener() {
    let newGameButton = document.getElementById("newGameButton");
    newGameButton.onclick = onNewGame;
}

function registerOnSignPlaceListener() {
    findAllCells().forEach(
        cellElement => cellElement.addEventListener("click", onPlaceSign)
    );
}

function removeOnSignPlaceListener() {
    findAllCells().forEach(
        cellElement => cellElement.removeEventListener("click", onPlaceSign)
    );
}

// Event handlers:

function startGame() {
    console.log("startGame...");
    let player0Name = window.prompt("Első játékos neve?");
    let player1Name = window.prompt("Második játékos neve?");
    let result = game.start(player0Name, player1Name);
    document.getElementById("player0Name").textContent = game.players[0].name;
    document.getElementById("player1Name").textContent = game.players[1].name;
    setMessage(result.message);
    highlightPlayer(game.currentPlayer);
    console.log(result);
}

function onNewGame() {
    clearAllCells();
    registerOnSignPlaceListener();
    clearCurrentPlayer();
    setTimeout(startGame);
    console.log("onNewGame ended...");
}

function onSuccessfulSignPlacement(event) {
    highlightPlayer(event.player); // aktuális játékos megjelölése
    setSignAt(event.y, event.x, event.sign); // elhelyezzük az X-et vagy a O-t
}

function onPlaceSign(event) {
    let element = event.currentTarget;
    let x = element.dataset.x;
    let y = element.dataset.y;
    try {
        let currentSign = game.currentPlayer.sign; // X vagy O
        
        let result = game.placeSign(y, x); // játék logika
        
        console.log(result, "\n" + game.toString());

        setMessage(result.message); // a visszakapott üzenetet megjelenítjük
        if (result.gameEnded) { // ha vége a játéknak
            removeOnSignPlaceListener();
            clearCurrentPlayer();
        }
    } catch (err) { // ha bármi hiba történik, itt lekezeljük
        setMessage(err.message); // megjelenítjük az oldalon
    }
}

registerNewGameListener();
clearAllCells();
clearCurrentPlayer();

game.addOnPlaceSignListener(onSuccessfulSignPlacement);

game.addOnPlaceSignListener(function(event) {
    console.log("on place sign happened", event);
});

console.log("glue.js betöltődött!");

